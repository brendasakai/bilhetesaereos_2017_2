package br.com.mauda.bilhetesAereos.bc;

import org.apache.commons.lang3.StringUtils;

import br.com.mauda.bilhetesAereos.dto.CiaAereaDTO;
import br.com.mauda.bilhetesAereos.exception.BilhetesAereosException;
import br.com.mauda.bilhetesAereos.model.CiaAerea;

public class CiaAereaBC extends PatternCrudBC<CiaAerea, CiaAereaDTO> {
	
	private static CiaAereaBC instance = new CiaAereaBC();

	private CiaAereaBC() {
	}

	public static CiaAereaBC getInstance() {
		return instance;
	}
	
	///////////////////////////////////////////////////////////////////
	// METODOS DE VALIDACAO
	///////////////////////////////////////////////////////////////////	
	
	@Override
	protected void validateForDataModification(CiaAerea object) {
		if(object == null){
			throw new BilhetesAereosException("ER0050");
		}
		
		if(StringUtils.isBlank(object.getNome())){
			throw new BilhetesAereosException("ER0051");
		}
	}

	@Override
	protected boolean validateForFindData(CiaAereaDTO object) {
		return object != null && 
				(object.getId() != null ||
				 StringUtils.isNotBlank(object.getNome()));
	}
}
