package br.com.mauda.bilhetesAereos.bc.crud;

import org.junit.Test;

import br.com.mauda.bilhetesAereos.bc.CiaAereaBC;
import br.com.mauda.bilhetesAereos.exception.BilhetesAereosException;
import br.com.mauda.bilhetesAereos.massaTestes.MassaTestesCiaAereaEnum;
import br.com.mauda.bilhetesAereos.model.crud.TesteCiaAereaModel;

public class TesteCiaAereaBC extends TesteCiaAereaModel {
	
	///////////////////////////////////////////
	// CONSTRUTORES
	///////////////////////////////////////////
	
	public TesteCiaAereaBC(MassaTestesCiaAereaEnum enumm) {
		super(enumm);
	}	
	
	//////////////////////////////////////////
	// METODOS DE TESTE JUNIT
	//////////////////////////////////////////
	
	/**
	 * Valida ciaAerea completa
	 */
	@Test
	@Override
	public void criar(){
		super.criar();
		CiaAereaBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida ciaAerea nula
	 */
	@Test(expected = BilhetesAereosException.class)
	public void validarCiaAereaNula(){
		CiaAereaBC.getInstance().insert(null);
	}
	
	/**
	 * Valida ciaAerea preenchida com espacos em branco os campos String
	 */
	@Test(expected = BilhetesAereosException.class)
	public void validarCiaAereaEspacosBranco(){
		object.setNome("                         ");

		CiaAereaBC.getInstance().insert(object);
	}	
	
	/**
	 * Valida ciaAerea sem o campo nome
	 */
	@Test(expected = BilhetesAereosException.class)
	public void validarCiaAereaSemNome(){
		object.setNome(null);
		CiaAereaBC.getInstance().insert(object);
	}
}