package br.com.mauda.bilhetesAereos.model.crud;

import org.junit.Before;
import org.junit.Test;

import br.com.mauda.bilhetesAereos.model.IdentifierInterface;
import br.com.mauda.bilhetesAereos.modificadores.AbstractCreator;
import br.com.mauda.bilhetesAereos.verificador.AbstractVerificator;

public abstract class AbstractCrudTestModel<T extends IdentifierInterface, E extends Enum<E>> {
	
	//////////////////////////////////////////
	// ATRIBUTOS
	//////////////////////////////////////////
	protected E objectEnum;
	protected E objectEnumTemp;
	protected T object;
	protected AbstractCreator<T, E> creator;
	protected AbstractVerificator<T, E> verificator;
	
	// ////////////////////////////////////////
	// CONSTRUTORES
	// ////////////////////////////////////////

	public AbstractCrudTestModel(E objectEnum) {
		System.out.println("\t\tValidando: " + objectEnum);
		this.objectEnum = objectEnum;
	}	
	
	// ////////////////////////////////////////
	// METODOS AUXILIARES
	// ////////////////////////////////////////

	@Before
	public void init() {
		this.object = creator.create(this.objectEnum);
	}

	// ////////////////////////////////////////
	// METODO DE TESTE JUNIT
	// ////////////////////////////////////////

	@Test
	public void criar() {
		// Verifica se os atributos estao preenchidos
		verificator.verify(object);

		// Alteracao dos atributos
		creator.update(object, objectEnumTemp);
		
		// Verifica se alteracao dos atributos esta sendo realizada
		verificator.verify(object, objectEnumTemp);
		
		//Volta os atributos, para ficar compativel com outros testes
		creator.update(object, objectEnum);
	}	
}